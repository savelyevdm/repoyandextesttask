package ru.developsdm.yamoney;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

/**
 * Robolectric tests... To be done.
 * <p/>
 * Created by Dan on 11.08.2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = "src/main/AndroidManifest.xml")
public class TopCategoryActivityTest {

    @Test
    public void testMethod() throws Exception {
        // no tests yet.
    }

}