package ru.developsdm.yamoney.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ru.developsdm.yamoney.R;
import ru.developsdm.yamoney.model.Category;
import ru.developsdm.yamoney.util.UtilLogging;

/**
 * Adapter for Category RecyclerView.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class CategoryAdapter extends EmptyErrorRecyclerView.Adapter<RecyclerView.ViewHolder> implements AdapterEnableController {

    /**
     * Static method to create & pass valuable data to adapter.
     *
     * @param fragment   host fragment
     * @param categories list of categories to manage.
     * @return new adapter.
     */
    public static CategoryAdapter newInstance(CategoryFragment fragment, @NonNull ArrayList<Category> categories) {

        CategoryAdapter adapter = new CategoryAdapter();
        adapter.setCategories(categories);
        adapter.setFragment(fragment);
        return adapter;

    }

    /**
     * Host fragment
     */
    private CategoryFragment fragment;
    /**
     * List of categories, stored in RecyclerView
     */
    private List<Category> categories;
    /**
     * Last item (shown on screen) position.
     */
    private int lastPositionNo;
    /**
     * Parent of RecyclerView.
     */
    private WeakReference<ViewGroup> parent;
    /**
     * Is list enabled.
     */
    private boolean mAllEnabled = true;

    /**
     * ViewHolder for progress bar.
     */
    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        // Animated layout (container of item).
        @InjectView(R.id.global_container)
        public RelativeLayout rlContainer;
        @InjectView(R.id.image)
        public ImageView ivImage;
        @InjectView(R.id.text_title)
        public TextView tvTitle;
        @InjectView(R.id.text_subtitle)
        public TextView tvSubtitle;

        public CategoryViewHolder(View v) {
            super(v);
            ButterKnife.inject(this, v);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int viewType) {

        if (parent == null)
            setParent(parentViewGroup);

        View v = LayoutInflater.from(getParent().getContext()).inflate(
                R.layout.item_category, getParent(), false);

        return new CategoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        final CategoryAdapter.CategoryViewHolder categoryViewHolder =
                (CategoryAdapter.CategoryViewHolder) viewHolder;

        // Animating VH entering
        setAnimation(categoryViewHolder.rlContainer, position);

        categoryViewHolder.rlContainer.setEnabled(isAllItemsEnabled());

        // Remembering last seen position (for animations)
        if (position > getLastPositionNo())
            setLastPositionNo(position);

        final Category category = getCategories().get(position);
        List<Category> childCategories = category.getChildren();

        // Title
        categoryViewHolder.tvTitle.setText(category.getTitle());

        if (childCategories != null && !childCategories.isEmpty()) { // folder
            // Image
            categoryViewHolder.ivImage.setImageResource(R.mipmap.ic_folder_grey600_24dp);
            // Subtitle
            StringBuilder sb = new StringBuilder();
            Iterator<Category> it = childCategories.iterator();
            while (it.hasNext()) {
                Category childCategory = it.next();
                sb.append(childCategory.getTitle());
                if (it.hasNext())
                    sb.append(", ");
            }
            categoryViewHolder.tvSubtitle.setVisibility(View.VISIBLE);
            categoryViewHolder.tvSubtitle.setText(sb.toString());

        } else { // item
            categoryViewHolder.ivImage.setImageResource(R.mipmap.ic_insert_drive_file_grey600_24dp);
            categoryViewHolder.tvSubtitle.setVisibility(View.GONE);
        }

        if (category.getChildren() != null && !category.getChildren().isEmpty()) {
            categoryViewHolder.rlContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    getFragment().onItemClicked(category.getYandexId(), category.getTitle());

                }
            });
        }

    }

    //==============================================================================================

    /**
     * If the bound view wasn't previously displayed on screen, it's animated
     */
    protected void setAnimation(View viewToAnimate, int position) {

        if (position > getLastPositionNo()) {

            Animator right = ObjectAnimator.ofFloat(viewToAnimate, "translationX", viewToAnimate.getWidth(), 0);
            AnimatorSet animSet = new AnimatorSet();
            animSet.playTogether(right);
            animSet.setDuration(200);
            animSet.start();

        }
    }

    /**
     * Release resources
     */
    public void releaseResources() {
        if (parent != null) {
            parent.clear();
            parent = null;
        }
        fragment = null;
    }

//    @Override
//    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
//        super.onViewAttachedToWindow(holder);
//
//    }

    @Override
    public boolean isAllItemsEnabled() {
        return mAllEnabled;
    }

    @Override
    public boolean getItemEnabled(int position) {
        return true;
    }

    public void setAllItemsEnabled(boolean enable) {
        mAllEnabled = enable;
        notifyItemRangeChanged(0, getItemCount());
    }

    //==============================================================================================

    @Override
    public int getItemCount() {
        return getCategories().size();
    }

    public int getLastPositionNo() {
        return lastPositionNo;
    }

    public void setLastPositionNo(int lastPositionNo) {
        this.lastPositionNo = lastPositionNo;
    }

    public ViewGroup getParent() {
        return parent.get();
    }

    public void setParent(ViewGroup parent) {
        this.parent = new WeakReference<ViewGroup>(parent);
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public CategoryFragment getFragment() {
        return fragment;
    }

    public void setFragment(CategoryFragment fragment) {
        this.fragment = fragment;
    }
}
