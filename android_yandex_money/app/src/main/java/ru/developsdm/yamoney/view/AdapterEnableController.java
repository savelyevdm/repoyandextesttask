package ru.developsdm.yamoney.view;

/**
 * RecyclerView Adapter interface. Need to disable list of categories while refreshing.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public interface AdapterEnableController {

    boolean isAllItemsEnabled();

    boolean getItemEnabled(int position);

}
