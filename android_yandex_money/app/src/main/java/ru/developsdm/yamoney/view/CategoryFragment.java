package ru.developsdm.yamoney.view;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import de.greenrobot.event.EventBus;
import ru.developsdm.yamoney.R;
import ru.developsdm.yamoney.component.DeepCategoryActivity;
import ru.developsdm.yamoney.component.TopCategoryActivity;
import ru.developsdm.yamoney.event.AbstractEvent;
import ru.developsdm.yamoney.event.SwipeRefreshStartedEvent;
import ru.developsdm.yamoney.event.SwipeRefreshFinishedEvent;
import ru.developsdm.yamoney.model.Category;
import ru.developsdm.yamoney.util.UtilLogging;

/**
 * Base class for Fragment with RecyclerView, EmptyView.
 * Created by Dan on 08.08.2015.
 */
public class CategoryFragment extends Fragment {

    public static final String TAG = CategoryFragment.class.getSimpleName();

    private static final String CATEGORIES_KEY = "categories";
    private static final String IS_TOP_LEVEL_KEY = "isTopLevel";

    /**
     * Vars
     */
    private CategoryAdapter mAdapter;
    private boolean refreshing = false;
    /**
     * Views
     */
    @InjectView(R.id.recycler_view)
    EmptyErrorRecyclerView mRecyclerView;
    @Nullable
    @Optional
    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.empty_view)
    View emptyView;
    @InjectView(R.id.error_view)
    View errorView;

    /**
     * Creates & inits new Fragment
     *
     * @param categories list of categories to show
     * @param isTopLevel are categories of top level.
     * @return new fragment instance.
     */
    public static CategoryFragment newInstance(@NonNull ArrayList<Category> categories, boolean isTopLevel) {
        CategoryFragment myFragment = new CategoryFragment();

        Bundle args = new Bundle();
        args.putSerializable(CATEGORIES_KEY, categories);
        args.putBoolean(IS_TOP_LEVEL_KEY, isTopLevel);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        EventBus.getDefault().register(this);
    }

    public void onEventMainThread(AbstractEvent event) {
        if (event instanceof SwipeRefreshFinishedEvent) {
            UtilLogging.logD("Refresh finished!");
            refreshing = false;
            mAdapter.setAllItemsEnabled(true);
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (mRecyclerView != null) {
            mRecyclerView.releaseResources();
            mRecyclerView = null;
        }
        mAdapter.releaseResources();
        mAdapter = null;
        mSwipeRefreshLayout = null;
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // isTopLevelActivity
        boolean isTopLevel = getArguments().getBoolean(CategoryFragment.IS_TOP_LEVEL_KEY);

        View view;
        if (isTopLevel)
            view = inflater.inflate(R.layout.fragment_categories_top, container, false);
        else
            view = inflater.inflate(R.layout.fragment_categories_deep, container, false);
        ButterKnife.inject(this, view);

        // RecyclerView.
        mRecyclerView.setEmptyView(emptyView);
        mRecyclerView.setErrorView(errorView);
        mRecyclerView.setHasFixedSize(true);

        // Layout
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Extract categories list from bundle
        ArrayList<Category> categories = (ArrayList<Category>)
                getArguments().getSerializable(CategoryFragment.CATEGORIES_KEY);
        if (categories == null)
            categories = new ArrayList<Category>();

        // Adapter
        mAdapter = CategoryAdapter.newInstance(this, categories);
        mRecyclerView.setAdapter(mAdapter);

        if (isTopLevel && mSwipeRefreshLayout != null) {
            // Swipe layout
            int color = getResources().getColor(android.R.color.black);
            mSwipeRefreshLayout.setColorSchemeColors(color, color, color, color);

            // Action on PullToRefresh gesture.
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshing = true;
                    mAdapter.setAllItemsEnabled(false);
                    EventBus.getDefault().post(new SwipeRefreshStartedEvent());
                }
            });
            if (refreshing && mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.setAllItemsEnabled(false);
                        mSwipeRefreshLayout.setRefreshing(true);
                    }
                });
            }

        }

        return view;
    }

    /**
     * Called when category clicked.
     *
     * @param itemYandexId yandexId for Category.
     * @param itemTitle    Title of clicked Category.
     */
    void onItemClicked(String itemYandexId, String itemTitle) {

        Intent newIntent = new Intent(getActivity(), DeepCategoryActivity.class);
        newIntent.setAction(TopCategoryActivity.ACTION_OPEN_DEEP_CATEGORY);
        newIntent.putExtra(TopCategoryActivity.EXTRA_PARENT_ID, itemYandexId);
        newIntent.putExtra(TopCategoryActivity.EXTRA_PARENT_TITLE, itemTitle);
        newIntent.putExtra(TopCategoryActivity.EXTRA_IS_TOP_LEVEL, false);
        getActivity().startActivity(newIntent);

    }

}
