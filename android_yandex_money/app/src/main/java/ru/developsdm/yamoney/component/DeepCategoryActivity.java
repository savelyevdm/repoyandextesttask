package ru.developsdm.yamoney.component;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ru.developsdm.yamoney.R;
import ru.developsdm.yamoney.db.HelperFactory;
import ru.developsdm.yamoney.model.Category;
import ru.developsdm.yamoney.view.CategoryFragment;

/**
 * Activity to represent categories on deep level in deep level of hierarchy.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class DeepCategoryActivity extends AppCompatActivity {

    private Dialog aboutDialog = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        String parentId = getIntent().getStringExtra(TopCategoryActivity.EXTRA_PARENT_ID);
        String parentTitle = getIntent().getStringExtra(TopCategoryActivity.EXTRA_PARENT_TITLE);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(parentTitle);
        }

        if (savedInstanceState == null) {

            List<Category> categories = HelperFactory.getHelper().getCategoryDAO().readCategories(parentId);

            FragmentManager fm = getFragmentManager();
            CategoryFragment fragment = CategoryFragment.newInstance((ArrayList<Category>) categories, false);
            fm.beginTransaction().replace(R.id.frame_container, fragment, CategoryFragment.TAG).commit();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_deep, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.action_about) {
            showAboutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Dialog showAboutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_about_title)
                .setMessage(R.string.dialog_about_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog dialog = builder.show();
        return dialog;

    }

    private void releaseResources() {
        if (aboutDialog != null) {
            aboutDialog.dismiss();
            aboutDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        releaseResources();
        super.onDestroy();
    }
}
