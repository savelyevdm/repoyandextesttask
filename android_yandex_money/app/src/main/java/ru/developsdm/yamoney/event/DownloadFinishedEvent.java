package ru.developsdm.yamoney.event;

import java.util.List;

import ru.developsdm.yamoney.model.Category;

/**
 * Event fired, when download of categories ends and app needs to react (changing views).
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class DownloadFinishedEvent extends AbstractEvent {

    /**
     * Result of download (success/fail)
     */
    private boolean success;
    /**
     * If failed - exception message
     */
    private String message;
    /**
     * If success - downloaded list of categories
     */
    private List<Category> categories;

    public DownloadFinishedEvent(boolean success, List<Category> categories, String message) {
        this.success = success;
        this.categories = categories;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
