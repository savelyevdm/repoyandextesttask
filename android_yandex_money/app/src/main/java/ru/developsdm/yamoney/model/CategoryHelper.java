package ru.developsdm.yamoney.model;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Class used for some modifications of Category hierarchy.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class CategoryHelper {

    /**
     * Categories hierarchy top level index.
     */
    public static final int TOP_LEVEL = 0;
    /**
     * Categories hierarchy top level parentId index.
     */
    public static final String TOP_LEVEL_PARENT_ID = "000000";

    /**
     * Sets yandexId to top level categories.
     *
     * @param categories list of categories.
     */
    public static void setYandexIdToTopCategories(@NonNull List<Category> categories) {
        int index = 1;
        for (Category category : categories) {
            String yaId = String.format("%06d", index);
            category.setYandexId(yaId);
            index++;
        }
    }

    /**
     * Recursively set level & parentId to list of categories & its children
     * (to properly save to db).
     * Need to be started with level = 0 & parentId = "000000".
     *
     * @param level      level of category.
     * @param parentId   yandexId of parent or zero, if top category.
     * @param categories list of categories to set.
     */
    public static void setLevelAndParentId(int level, @NonNull String parentId,
                                           @NonNull List<Category> categories) {
        for (Category category : categories) {
            category.setLevel(level);
            category.setParentId(parentId);
            List<Category> children = category.getChildren();
            if (children != null && !children.isEmpty()) {
                setLevelAndParentId(level + 1, category.getYandexId(), children);
            }
        }
    }

}
