package ru.developsdm.yamoney.connection;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import ru.developsdm.yamoney.model.Category;
import rx.Observable;

/**
 * Api to be used by Retrofit.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public interface RestApi {

    /**
     * Endpoints
     */
    String ENDPOINT_YANDEX = "https://money.yandex.ru/api";
    String ENDPOINT_TEST = "http://topsource.u0094765.cp.regruhosting.ru";

    @GET("/categories-list")
    Observable<List<Category>> getYandexCategories();

    @GET("/yandex/json.php")
    Observable<List<Category>> getTestCategories();

    @GET("/yandex/json20.php")
    Observable<List<Category>> getNullCategories();

}
