package ru.developsdm.yamoney.event;

/**
 * Event fired, when user started the download process by SwipeRefreshLayout.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class SwipeRefreshStartedEvent extends AbstractEvent {
}
