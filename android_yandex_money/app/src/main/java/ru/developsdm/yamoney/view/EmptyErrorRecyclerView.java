package ru.developsdm.yamoney.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import ru.developsdm.yamoney.util.UtilLogging;

/**
 * RecyclerView with ability to show errors and handle empty lists.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class EmptyErrorRecyclerView extends RecyclerView {

    private View mEmptyView;

    private View mErrorView;

    private boolean isError;

    private int mVisibility;

    private RecyclerView.AdapterDataObserver mObserver = new RecyclerView.AdapterDataObserver() {

        @Override
        public void onChanged() {
            updateEmptyView();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            updateEmptyView();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            updateEmptyView();
        }

    };

    public EmptyErrorRecyclerView(Context context) {
        super(context);
        mVisibility = getVisibility();
    }

    public EmptyErrorRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mVisibility = getVisibility();
    }

    public EmptyErrorRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mVisibility = getVisibility();
    }

    @Override
    public void setAdapter(Adapter adapter) {
        Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(mObserver);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mObserver);
        }
        updateEmptyView();
    }

    public void releaseResources() {
        if (getAdapter() != null) {
            getAdapter().unregisterAdapterDataObserver(mObserver);
        }
        mEmptyView = null;
        mErrorView = null;
        mObserver = null;
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        mVisibility = visibility;
        updateErrorView();
        updateEmptyView();
    }

    private void updateEmptyView() {
        if (mEmptyView != null && getAdapter() != null) {

            boolean showEmptyView = getAdapter().getItemCount() == 0;
            UtilLogging.logD("Update state of RecyclerView | Show empty view ? " + showEmptyView);
            boolean emptyViewVisibility = showEmptyView && !shouldShowErrorView() && mVisibility == VISIBLE;
            mEmptyView.setVisibility(emptyViewVisibility ? VISIBLE : GONE);
            if (emptyViewVisibility)
                ObjectAnimator.ofFloat(mEmptyView, "alpha", 0f, 1f).start();
            boolean mainViewVisibility = !showEmptyView && !shouldShowErrorView() && mVisibility == VISIBLE;

        }
    }

    private void updateErrorView() {
        if (mErrorView != null) {
            mErrorView.setVisibility(shouldShowErrorView() && mVisibility == VISIBLE ? VISIBLE : GONE);
        }
    }

    private boolean shouldShowErrorView() {
        if (mErrorView != null && isError) {
            return true;
        }
        return false;
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
        updateEmptyView();
    }

    public void setErrorView(View errorView) {
        mErrorView = errorView;
        updateErrorView();
        updateEmptyView();
    }

    public void showErrorView() {
        isError = true;
        updateErrorView();
        updateEmptyView();
    }

    public void hideErrorView() {
        isError = false;
        updateErrorView();
        updateEmptyView();
    }

}
