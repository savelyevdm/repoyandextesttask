package ru.developsdm.yamoney.event;

/**
 * Event fired, when download of new categories finished & app must turn SwipeRefreshLayout off.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class SwipeRefreshFinishedEvent extends AbstractEvent {
}
