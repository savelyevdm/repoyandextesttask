package ru.developsdm.yamoney.component;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import ru.developsdm.yamoney.R;
import ru.developsdm.yamoney.connection.RequestBuilder;
import ru.developsdm.yamoney.connection.RestApi;
import ru.developsdm.yamoney.db.HelperFactory;
import ru.developsdm.yamoney.event.AbstractEvent;
import ru.developsdm.yamoney.event.DownloadFinishedEvent;
import ru.developsdm.yamoney.event.SwipeRefreshStartedEvent;
import ru.developsdm.yamoney.event.SwipeRefreshFinishedEvent;
import ru.developsdm.yamoney.model.Category;
import ru.developsdm.yamoney.model.CategoryHelper;
import ru.developsdm.yamoney.util.UtilInternet;
import ru.developsdm.yamoney.util.UtilLogging;
import ru.developsdm.yamoney.view.CategoryFragment;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Activity to show & download categories.
 */
public class TopCategoryActivity extends AppCompatActivity {

    /**
     * Intent keys to open DeepCategoryActivity
     */
    public final static String ACTION_OPEN_DEEP_CATEGORY = "ru.developsdm.yamoney.ACTION_OPEN_DEEP_CATEGORY";
    public final static String EXTRA_PARENT_ID = "ru.developsdm.yamoney.EXTRA_PARENT_ID";
    public final static String EXTRA_PARENT_TITLE = "ru.developsdm.yamoney.EXTRA_PARENT_TITLE";
    public final static String EXTRA_IS_TOP_LEVEL = "ru.developsdm.yamoney.IS_TOP_LEVEL_ACTIVITY";
    /**
     * Retain keys
     */
    private final static String KEY_PROGRESS_DIALOG_IS_SHOWN = "key_progress_dialog_is_shown";
    private final static String KEY_ABOUT_DIALOG_IS_SHOWN = "key_about_dialog_is_shown";
    private final static String KEY_SOURCE_DIALOG_IS_SHOWN = "key_source_dialog_is_shown";
    /**
     * Dialogs
     */
    private ProgressDialog pDialog = null;
    private Dialog aboutDialog = null;
    private Dialog sourceDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        // Subscribe EventBus
        EventBus.getDefault().register(this);

        if (savedInstanceState == null) {

            // First launch - init ORMLite.
            HelperFactory.setHelper(getApplicationContext());

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String preferenceKey = getResources().getString(R.string.preference_data_in_db_key);
            boolean hasCategories = prefs.getBoolean(preferenceKey, false);
            if (hasCategories) {

                List<Category> categories = HelperFactory.getHelper().getCategoryDAO()
                        .readCategories(CategoryHelper.TOP_LEVEL_PARENT_ID);
                showCategoriesFragment(categories);

            } else {
                // download categories tree.
                if (UtilInternet.isConnected(getApplicationContext())) {
                    // try to download categories tree
                    downloadCategories(true);
                } else {
                    UtilLogging.toast(R.string.toast_no_internet_connection);
                }
            }

        } else { // restore dialogs after rotate

            String key = TopCategoryActivity.KEY_PROGRESS_DIALOG_IS_SHOWN;
            if (savedInstanceState.containsKey(key) && savedInstanceState.getBoolean(key))
                showProgressDialog();
            key = TopCategoryActivity.KEY_ABOUT_DIALOG_IS_SHOWN;
            if (savedInstanceState.containsKey(key) && savedInstanceState.getBoolean(key))
                showAboutDialog();
            key = TopCategoryActivity.KEY_SOURCE_DIALOG_IS_SHOWN;
            if (savedInstanceState.containsKey(key) && savedInstanceState.getBoolean(key))
                showSourceDialog();

        }

    }

    @Override
    protected void onDestroy() {
        releaseResources();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (pDialog != null && pDialog.isShowing())
            outState.putBoolean(TopCategoryActivity.KEY_PROGRESS_DIALOG_IS_SHOWN, true);
        if (aboutDialog != null && aboutDialog.isShowing())
            outState.putBoolean(TopCategoryActivity.KEY_ABOUT_DIALOG_IS_SHOWN, true);
        if (sourceDialog != null && sourceDialog.isShowing())
            outState.putBoolean(TopCategoryActivity.KEY_SOURCE_DIALOG_IS_SHOWN, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_top, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about) {
            showAboutDialog();
            return true;
        }
        if (id == R.id.action_refresh) {
            downloadCategories(true);
            return true;
        }
        if (id == R.id.action_source) {
            showSourceDialog();
            return true;
        }
        if (id == R.id.action_show_db_count) {
            showDBCount();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //==============================================================================================

    /**
     * Release resources when destroying activity.
     */
    private void releaseResources() {
        // Release EventBus
        EventBus.getDefault().unregister(this);
        // Release ORMLite.
        if (!isChangingConfigurations()) {
            HelperFactory.releaseHelper();
        }
        if (pDialog != null) {
            if (pDialog.isShowing())
                pDialog.dismiss();
            pDialog = null;
        }
        if (aboutDialog != null) {
            aboutDialog.dismiss();
            aboutDialog = null;
        }
        if (sourceDialog != null) {
            sourceDialog.dismiss();
            sourceDialog = null;
        }
    }

    /**
     * Downloads categories json from server, parses it & saves to DB.
     *
     * @param createDialog if true - creates progress dialog, when loading.
     */
    private void downloadCategories(boolean createDialog) {

        if (createDialog) {
            showProgressDialog();
        }

        String sourceKey = getResources().getString(R.string.preference_data_source);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int source = prefs.getInt(sourceKey, 0);

        RestApi apiInstance = RequestBuilder.build(source);
        Observable<List<Category>> observable = null;

        switch (source) {
            case 0:
            default:
                observable = apiInstance.getYandexCategories();
                break;
            case 1:
                observable = apiInstance.getTestCategories();
                break;
            case 2:
                observable = apiInstance.getNullCategories();
                break;
        }

        observable
                .map(new Func1<List<Category>, List<Category>>() {
                    @Override
                    public List<Category> call(List<Category> categories) {
                        CategoryHelper.setYandexIdToTopCategories(categories);
                        CategoryHelper.setLevelAndParentId(CategoryHelper.TOP_LEVEL,
                                CategoryHelper.TOP_LEVEL_PARENT_ID, categories);
                        HelperFactory.getHelper().getCategoryDAO().saveCategories(categories);
                        return categories;
                    }
                })
                .subscribeOn(Schedulers.io()) // schedule work in a background io thread.
                .observeOn(AndroidSchedulers.mainThread()) // schedule next/complete/error on the androids main thread.
                .subscribe(new Subscriber<List<Category>>() { // kick off the actual work.
                    @Override
                    public void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        EventBus.getDefault().post(new SwipeRefreshFinishedEvent());
                        EventBus.getDefault().post(new DownloadFinishedEvent(false, null, e.getMessage()));
                    }

                    @Override
                    public void onNext(List<Category> categories) {
                        EventBus.getDefault().post(new SwipeRefreshFinishedEvent());
                        EventBus.getDefault().post(new DownloadFinishedEvent(true, categories, null));
                    }
                });

    }

    /**
     * Show db count.
     */
    private void showDBCount() {
        try {
            long count = HelperFactory.getHelper().getCategoryDAO().getAllCategoriesCount();
            String text = getResources().getString(R.string.toast_db_count);
            UtilLogging.toast(String.format(text, count));
        } catch (Exception e) {
            UtilLogging.logE(e.getMessage(), e);
        }
    }

    /**
     * Show about dialog.
     */
    private void showAboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_about_title)
                .setMessage(R.string.dialog_about_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    /**
     * Show dialog to choose source server from where download categories.
     */
    private void showSourceDialog() {

        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Strings to Show In Dialog with Radio Buttons
        final String[] items = getResources().getStringArray(R.array.dialog_source_values);
        final String currentSourceKey = getResources().getString(R.string.preference_data_source);
        final int currentSource = prefs.getInt(currentSourceKey, 0);

        // Creating and Building the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_source_title);
        builder.setSingleChoiceItems(items, currentSource, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                prefs.edit().putInt(currentSourceKey, item).apply();
                sourceDialog.dismiss();

            }
        });
        sourceDialog = builder.create();
        sourceDialog.show();
    }

    private void showProgressDialog() {

        if (pDialog != null) {
            if (pDialog.isShowing())
                pDialog.dismiss();
            pDialog = null;
        }
        // progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage(getResources().getString(R.string.progress_dialog_downloading));
        pDialog.show();

    }

    private void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * Creates fragment to show list of categories.
     *
     * @param categories list of categories to show.
     */
    private void showCategoriesFragment(List<Category> categories) {
        FragmentManager fm = getFragmentManager();
        CategoryFragment fragment = CategoryFragment.newInstance((ArrayList<Category>) categories, true);
        fm.beginTransaction().replace(R.id.frame_container, fragment, CategoryFragment.TAG).commit();
    }

    /**
     * EventBus events.
     *
     * @param event Input event
     */
    public void onEventMainThread(AbstractEvent event) {
        if (event instanceof SwipeRefreshStartedEvent) {

            downloadCategories(false);

        }
        if (event instanceof DownloadFinishedEvent) {

            DownloadFinishedEvent rfe = (DownloadFinishedEvent) event;
            if (rfe.isSuccess()) {
                // mark all categories loaded.
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String preferenceKey = getResources().getString(R.string.preference_data_in_db_key);
                prefs.edit().putBoolean(preferenceKey, true).apply();
                // show categories in fragment
                showCategoriesFragment(rfe.getCategories());
                // show message
                UtilLogging.logD("Download finished. Success");
                UtilLogging.toast(R.string.toast_download_succeed);
            } else {
                String message = String.format(getResources()
                        .getString(R.string.toast_download_failed), rfe.getMessage());
                UtilLogging.logE(rfe.getMessage());
                UtilLogging.toast(message);
            }
            hideProgressDialog();
        }
    }
}
