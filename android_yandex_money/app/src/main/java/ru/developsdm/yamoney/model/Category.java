package ru.developsdm.yamoney.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.List;

/**
 * Category entity. Used by Gson & ORMLite.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
@DatabaseTable(tableName = "categories")
public class Category implements Serializable {

    /**
     * DB field names.
     */
    public final static String FIELD_ID = "_id";
    public final static String FIELD_YANDEX_ID = "yandex_id";
    public final static String FIELD_TITLE = "title";
    public final static String FIELD_LEVEL = "level";
    public final static String FIELD_PARENT_ID = "parent_id";

    /**
     * DB id.
     */
    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    @SerializedName("_id") // for gson
    private int id;
    /**
     * Yandex id.
     */
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = FIELD_YANDEX_ID)
    @SerializedName("id") // for gson
    private String yandexId;
    /**
     * Title of category.
     */
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = FIELD_TITLE)
    private String title;
    /**
     * Level in tree. Top level is 'zero'.
     */
    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER, columnName = FIELD_LEVEL)
    private int level;
    //
    /**
     * Id of elements parent (by yandex id).
     * If no parent - it is 0.
     */
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = FIELD_PARENT_ID)
    private String parentId;
    /**
     * List of element's children (list of categories).
     */
    @DatabaseField(persisted = false)
    @SerializedName("subs") // for gson
    private List<Category> children;

    public Category() {
    }

    @Override
    public String toString() {
        int count = (children != null && !children.isEmpty()) ? children.size() : 0;
        return String.format("Category {id=%d, yaId=%s, title=%s, level=%d, parentId=%s, subsCount=%d}",
                getId(), getYandexId(), getTitle(), getLevel(), getParentId(), count);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYandexId() {
        return yandexId;
    }

    public void setYandexId(String yandexId) {
        this.yandexId = yandexId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }
}
