package ru.developsdm.yamoney.connection;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import ru.developsdm.yamoney.model.Category;

/**
 * Retrofit request builder.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class RequestBuilder {

    private static Gson gson = null;
    private static RequestInterceptor requestInterceptor = null;
    private static OkHttpClient okHttpClient = null;

    /**
     * Builds request api.
     *
     * @param source source server
     * @return request api
     */
    public static RestApi build(int source) {

        String endpoint = (source == 0) ? RestApi.ENDPOINT_YANDEX : RestApi.ENDPOINT_TEST;

        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient();
            okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        }

        if (gson == null) {
            gson = getGson();
        }

        if (requestInterceptor == null) {
            requestInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("User-Agent", "ru.developsdm.yamoney");
                    request.addHeader("Accept", "application/json");
                }
            };
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter.create(RestApi.class);

    }

    public static Gson getGson() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return gson;
    }
}
