package ru.developsdm.yamoney.db;

import android.support.annotation.NonNull;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.developsdm.yamoney.model.Category;
import ru.developsdm.yamoney.util.UtilLogging;

/**
 * DAO for Category class.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class CategoryDAO extends BaseDaoImpl<Category, Integer> {

    protected CategoryDAO(ConnectionSource connectionSource, Class<Category> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    /**
     * Getting count of db table records.
     *
     * @return count
     * @throws SQLException e
     */
    public long getAllCategoriesCount() throws SQLException {
        return this.countOf();
    }

    /**
     * Getting 2 levels of categories. First method queries for level with given parentId,
     * then queries for their children.
     *
     * @param parentId parent id of top level category
     * @return list of categories
     * @throws SQLException e
     */
    public List<Category> getCategoriesWithChildren(@NonNull String parentId) throws SQLException {
        List<Category> topCategories = getCategories(parentId);
        if (topCategories != null && !topCategories.isEmpty()) {
            for (Category topCategory : topCategories) {
                List<Category> childCategories = getCategories(topCategory.getYandexId());
                if (childCategories != null && !childCategories.isEmpty()) {
                    topCategory.setChildren(childCategories);
                }
            }
        }
        return topCategories;
    }

    /**
     * Getting 1 level of categories with parentId.
     *
     * @param parentId parent id of top level category
     * @return list of categories
     * @throws SQLException e
     */
    public List<Category> getCategories(@NonNull String parentId) throws SQLException {

        QueryBuilder<Category, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Category.FIELD_PARENT_ID, parentId);
        PreparedQuery<Category> preparedQuery = queryBuilder.prepare();
        List<Category> categoryList = query(preparedQuery);
        return categoryList;

    }

    /**
     * Clears all records from database table.
     *
     * @throws SQLException e
     */
    public void clearAllCategories() throws SQLException {

        UtilLogging.logD("Delete all db records started...");
        long t1 = System.currentTimeMillis();
        int result = TableUtils.clearTable(getConnectionSource(), Category.class);
        long t2 = System.currentTimeMillis();
        UtilLogging.logD(String.format("Deleted [%d] records", result));
        UtilLogging.logD(String.format("Delete took %d(ms)", (t2 - t1)));

    }

    //==============================================================================================

    /**
     * Wrapper method.
     * Reads from DB 2 levels of categories : categories with given parentID & their children.
     *
     * @return List of categories with children (2 levels depth together).
     */
    public List<Category> readCategories(@NonNull String parentId) {
        List<Category> categories = new ArrayList<Category>();
        try {
            long t1 = System.currentTimeMillis();
            List<Category> tempCategories =
                    HelperFactory.getHelper().getCategoryDAO().getCategoriesWithChildren(parentId);
            if (tempCategories != null && !tempCategories.isEmpty())
                categories.addAll(tempCategories);
            long t2 = System.currentTimeMillis();
            UtilLogging.logD(String.format("Reading from database took %d(ms)", (t2 - t1)));
        } catch (SQLException sqle) {
            UtilLogging.logE("SQLException while reading 2 levels of categories ", sqle);
        } catch (Exception e) {
            UtilLogging.logE("Exception while reading 2 levels of categories ", e);
        }
        return categories;
    }

    /**
     * Wrapper method.
     * Clears all records & then saves new categories.
     *
     * @param categories list of categories to save.
     */
    public void saveCategories(@NonNull List<Category> categories) {

        try {
            // clear records
            clearAllCategories();
            // saving records recursively.
            UtilLogging.logD("Save to db started");
            long t1 = System.currentTimeMillis();
            setAutoCommit(false);
            storeCategories(categories);
            setAutoCommit(true);
            long t2 = System.currentTimeMillis();
            UtilLogging.logD(String.format("Save to database took %d(ms)", (t2 - t1)));
        } catch (SQLException sqle) {
            UtilLogging.logE("SQLException while cleaning or saving categories", sqle);
        } catch (Exception e) {
            UtilLogging.logE("Exception while cleaning or saving categories", e);
        }

    }

    /**
     * Recursively saves to db the list of categories.
     *
     * @param categories list of categories to save.
     * @throws SQLException e
     */
    private void storeCategories(@NonNull List<Category> categories) throws SQLException {
        for (Category category : categories) {
            create(category);
            UtilLogging.logD(String.format("category %s created!", category.toString()));

            List<Category> children = category.getChildren();
            if (children != null && !children.isEmpty()) {
                storeCategories(children);
            }
        }
    }

}
