package ru.developsdm.yamoney.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

import ru.developsdm.yamoney.Data;

/**
 * Logging utils.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class UtilLogging {

    /**
     * Debug constant. Must be disabled in release mode.
     */
    private static final boolean DEBUG = false;

    /**
     * App log tag.
     */
    public static final String LOG_TAG = "YandexTest";

    /**
     * Debug levels.
     */
    public enum Lvl {
        D, I, W, E
    }

    ;

    public static void log(Lvl l, String msg) {
        if (!DEBUG)
            return;
        if (msg == null)
            return;
        switch (l) {
            case D:
                Log.d(LOG_TAG, msg);
                break;
            case I:
                Log.i(LOG_TAG, msg);
                break;
            case W:
                Log.w(LOG_TAG, msg);
                break;
            case E:
                Log.e(LOG_TAG, msg);
                break;
            default:
                Log.d(LOG_TAG, msg);
                break;
        }
    }

    public static void logD(String msg) {
        log(Lvl.D, msg);
    }

    public static void logI(String msg) {
        log(Lvl.I, msg);
    }

    public static void logW(String msg) {
        log(Lvl.W, msg);
    }

    public static void logE(String msg) {
        log(Lvl.E, msg);
    }

    public static void logE(String msg, Throwable e) {
        log(Lvl.E, msg);
        printStackTrace(e);
    }

    /**
     * Prints Android exceptions in Eclipse console style.
     *
     * @param e Exception raised.
     */
    public static void printStackTrace(Throwable e) {
        if (e == null)
            return;
        logE("Caught exception: [ " + e.getClass().getName() + " ] with message: [ " + e.getMessage() + " ]");
        logE("MyStackTrase:");
        StackTraceElement[] elems = e.getStackTrace();
        for (StackTraceElement el : elems) {
            String s = String.format(Locale.US, "    at %s.%s(%s:%d)",
                    el.getClassName(), el.getMethodName(), el.getFileName(), el.getLineNumber());
            Log.e(LOG_TAG, s);
        }
    }

    public static void log(Lvl l, int resId) {
        Context ctx = Data.getContext();
        String msg = (ctx == null) ? "" : ctx.getResources().getString(resId);
        log(l, msg);
    }

    public static void toast(String msg) {
        if (Data.getContext() != null)
            Toast.makeText(Data.getContext(), msg == null ? "" : msg, Toast.LENGTH_SHORT).show();
    }

    public static void toast(int resId) {
        if (Data.getContext() != null)
            Toast.makeText(Data.getContext(), resId, Toast.LENGTH_SHORT).show();
    }

    public static void toastLong(String msg) {
        if (Data.getContext() != null)
            Toast.makeText(Data.getContext(), msg == null ? "" : msg, Toast.LENGTH_LONG).show();
    }

    public static void toastLong(int resId) {
        if (Data.getContext() != null)
            Toast.makeText(Data.getContext(), resId, Toast.LENGTH_LONG).show();
    }

}
