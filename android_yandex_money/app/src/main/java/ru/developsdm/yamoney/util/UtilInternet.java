package ru.developsdm.yamoney.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Connectivity utils.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class UtilInternet {

    /**
     * Checks if wi-fi is connected.
     *
     * @return connectivity state.
     */
    private static boolean isConnectedWiFi(Context context) {

        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    /**
     * Checks if mobile network (3g/4g) is connected.
     *
     * @return connectivity state.
     */
    private static boolean isConnectedMobile(Context context) {

        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mMobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return mMobile.isConnected();
    }

    /**
     * Checks, if any connection is available.
     *
     * @return connectivity state.
     */
    public static boolean isConnected(Context context) {
        return (isConnectedWiFi(context) || isConnectedMobile(context));
    }


}
