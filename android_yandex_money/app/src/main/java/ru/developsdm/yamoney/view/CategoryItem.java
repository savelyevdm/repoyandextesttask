package ru.developsdm.yamoney.view;

import android.support.annotation.NonNull;

import java.util.Locale;

import ru.developsdm.yamoney.model.Category;

/**
 * Item for recycler view adapter.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class CategoryItem {

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    private Category category;

    //==============================================================================================

    public CategoryItem(@NonNull Category category) {
        this.category = category;
    }

    //==============================================================================================

    @Override
    public String toString() {
        return String.format(Locale.US, "Item: [%s]", getCategory());
    }

}
