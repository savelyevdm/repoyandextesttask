package ru.developsdm.yamoney.component;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.lang.ref.WeakReference;

import ru.developsdm.yamoney.R;

/**
 * Yandex Test Task Application
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class CategoryApplication extends Application {

    private static WeakReference<Context> context;

    public void onCreate() {
        super.onCreate();
        CategoryApplication.context = new WeakReference<Context>(getApplicationContext());
        writeDefaultPreferences();
    }

    /**
     * Writes default SharedPreferences on startup.
     */
    private void writeDefaultPreferences() {
        String preferenceKey = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        // data existence in database
        preferenceKey = getResources().getString(R.string.preference_data_in_db_key);
        if (!prefs.contains(preferenceKey)) {
            prefs.edit().putBoolean(preferenceKey, false).apply();
        }
        // data source
        preferenceKey = getResources().getString(R.string.preference_data_source);
        if (!prefs.contains(preferenceKey)) {
            prefs.edit().putInt(preferenceKey, 0).apply();
        }

    }

    public static Context getContext() {
        return CategoryApplication.context.get();
    }
}
