package ru.developsdm.yamoney.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ru.developsdm.yamoney.model.Category;
import ru.developsdm.yamoney.util.UtilLogging;

/**
 * ORMLite DatabaseHelper
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    /**
     * DB name
     */
    private static final String DATABASE_NAME = "yamoney.db";
    /**
     * DB version
     */
    private static final int DATABASE_VERSION = 1;

    private CategoryDAO categoryDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Category.class);
            UtilLogging.logD(String.format("Database %s created!", db.getPath()));
        } catch (SQLException e) {
            UtilLogging.logE("error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        // do nothing
    }

    public CategoryDAO getCategoryDAO() {
        if (categoryDao == null) {
            try {
                categoryDao = new CategoryDAO(getConnectionSource(), Category.class);
            } catch (SQLException sqle) {
                throw new RuntimeException("Can't get DAO object");
            }
        }
        return categoryDao;
    }

    @Override
    public void close() {
        categoryDao = null;
        super.close();
    }
}
