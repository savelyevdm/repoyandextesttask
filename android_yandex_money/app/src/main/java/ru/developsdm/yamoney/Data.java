package ru.developsdm.yamoney;

import android.content.Context;

import ru.developsdm.yamoney.component.CategoryApplication;

/**
 * Data object.
 * <p/>
 * Created by Dan on 08.08.2015.
 */
public class Data {

    public static Context getContext() {
        return CategoryApplication.getContext();
    }

}